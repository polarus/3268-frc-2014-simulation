import ode
import math

# Constants
lbs2g = 453.592
ft2m = 0.3048
m2ft = 1.0/ft2m
in2m = 0.0254
deg2rad = math.pi / 180.0

ballMass = 2.75 * lbs2g
ballDiameter = 2.0 * ft2m
ballDragArea = math.pi * ballDiameter * ballDiameter / 2.0

robotDistance = 55 * ft2m
shooterHeight = 60 * in2m # height of ball's lowest point when it leaves the robot
shooterAngle = 45 * deg2rad # Angle between horizon and ball velocity vector when it leaves the shooter (2d)
ballHeight = shooterHeight + ballDiameter/2.0

goalHeight = 7.0 * ft2m

airRho = 1.2041 
ballC_d = 0.47

# calculate cortesian (x,y)python tuple to list components for given polar (radius, angle) coordinates
def polar2cortesianxy0( radius, angle ):
	x = radius * math.cos(angle)
	y = radius * math.sin(angle)
	z = 0.0
	return (x,y,z)

#calculates polar coordinates (radius,angle) for given cortesian (x,y) coordinates
def cortesianxy02polar( x, y, z ):
	radius = math.sqrt(x*x + y*y)
	angle = math.atan2( y, x )
	return (radius,angle)

def getDragForce ( rho, (u,v,w), c_d, dragArea ):
	dragConstant = -0.5 * rho * c_d * dragArea
	dragForce = ( dragConstant*u*u, dragConstant*v*v, dragConstant*w*w )
	return dragForce
	
# Create world object
world = ode.World()
world.setGravity( (0,-9.81,0) )

# Add ball to the world
ball = ode.Body(world)
M = ode.Mass()
M.setSphereTotal( ballMass, ballDiameter/2.0 )
ball.setMass(M)

ball.setPosition( (-1.0*robotDistance, ballHeight, 0) )

#This will vary during simulation until solution is found
ballVelocity = 13.35 #m/s, initial velocity

ballVelocityVector = polar2cortesianxy0( ballVelocity, shooterAngle )
ball.setLinearVel( ballVelocityVector )

# Simulation
total_time = 0.0 #s
dt = 0.001
x,y,z = ball.getPosition()
u,v,w = ball.getLinearVel()
while total_time < 30.0 and y>=0 and x<=0: 
	print "%1.2fsec: pos=(%6.3f, %6.3f, %6.3f)  vel=(%6.3f, %6.3f, %6.3f)" % \
		(total_time, x*m2ft, y*m2ft, z*m2ft, u,v,w)
	dragForce = getDragForce ( airRho, (u,v,w), ballC_d, ballDragArea )
	ball.addForce( dragForce )
	world.step(dt)
	total_time+=dt
	x,y,z = ball.getPosition()
	u,v,w = ball.getLinearVel()
