Simulation scripts for 2014 FRC game

		  bin/ball.py - ball trajectory simulator.
						Assumes the following:
						Mass = 2.75lbs
						Diameter = 2'
						rough surface, c_d = 0.47
						air temperature = 20C
						dry air
					  
						Takes distance to the goal in ft and launch angle as an input
					  
						Calculates launch velocity required to hit the high goal

						Uses PyODE library with drag adjustment at each simulation step
